package es.estech.coordinator.toolbarsola

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import es.estech.coordinator.CheeseAdapter
import es.estech.coordinator.Cheeses
import es.estech.coordinator.databinding.ToolbarScrollBinding

class ToolbarScroll : AppCompatActivity() {

    private lateinit var binding: ToolbarScrollBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ToolbarScrollBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        setSupportActionBar(binding.toolbar)
        title = "Toolbar scroll"
    }

    private fun setupRecyclerView() {
        val recyclerView = binding.recyclerview
        val listaQuesos = Cheeses.getRandomSublist(30)

        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = CheeseAdapter(listaQuesos)
    }

}