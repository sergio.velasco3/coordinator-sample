package es.estech.coordinator.parallax

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.estech.coordinator.databinding.ParallaxBinding

class ParallaxEffect : AppCompatActivity() {

    private lateinit var binding: ParallaxBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ParallaxBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        title = "Parallax effect"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }
}