package es.estech.coordinator.tablayout

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import es.estech.coordinator.R
import es.estech.coordinator.databinding.TablayoutActivityBinding

class ScrollTabLayout : AppCompatActivity() {

    private lateinit var binding: TablayoutActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TablayoutActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        title = "Toolbar Scroll"


        val fragments = ArrayList<Fragment>()
        fragments.add(Tab1Fragment())
        fragments.add(Tab2Fragment())
        fragments.add(Tab3Fragment())

        binding.viewpager.adapter = ViewPagerAdapter(this, fragments)

        TabLayoutMediator(binding.tablayout, binding.viewpager) { tab, position ->
            tab.text = "Fragment ${position + 1}"
            tab.icon = if (position == 1)
                ContextCompat.getDrawable(this, R.drawable.ic_launcher_foreground)
            else
                ContextCompat.getDrawable(this, R.drawable.ic_done_black_24dp)
        }.attach()
    }
}