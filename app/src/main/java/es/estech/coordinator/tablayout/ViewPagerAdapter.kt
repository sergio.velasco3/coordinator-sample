package es.estech.coordinator.tablayout

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(
    activity: AppCompatActivity,
    val listado: ArrayList<Fragment>
): FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return listado.size
    }

    override fun createFragment(position: Int): Fragment {
        return listado[position]
    }
}