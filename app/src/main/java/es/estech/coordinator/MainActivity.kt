package es.estech.coordinator

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import es.estech.coordinator.bottomappbar.BottomAppBarActivity
import es.estech.coordinator.collapsingconfab.CollapseYFab
import es.estech.coordinator.collapsingtoolbar.CollapseCase1
import es.estech.coordinator.collapsingtoolbar.CollapseCase2
import es.estech.coordinator.collapsingtoolbar.CollapseCase3
import es.estech.coordinator.collapsingtoolbar.CollapseCase4
import es.estech.coordinator.databinding.ActivityMainBinding
import es.estech.coordinator.floatingactionbutton.FabConSnackYScroll
import es.estech.coordinator.floatingactionbutton.FabEntreVistas
import es.estech.coordinator.floatingactionbutton.FabExpandido
import es.estech.coordinator.parallax.ParallaxEffect
import es.estech.coordinator.tablayout.ScrollTabLayout
import es.estech.coordinator.toolbarsola.ToolbarEnterAlways
import es.estech.coordinator.toolbarsola.ToolbarScroll

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        with(binding.content) {
            button1.setOnClickListener {
                fabAlertDialog()
            }

            button3.setOnClickListener {
                openActivity(BottomAppBarActivity::class.java)
            }

            button4.setOnClickListener {
                toolbarAlertDialog()
            }

            button6.setOnClickListener {
                colllapsingAlertDialog()
            }

            button10.setOnClickListener {
                openActivity(ScrollTabLayout::class.java)
            }

            button11.setOnClickListener {
                openActivity(CollapseYFab::class.java)
            }

            button12.setOnClickListener {
                openActivity(ParallaxEffect::class.java)
            }
        }
    }

    private fun fabAlertDialog() {
        val lista =
            arrayOf("FAB hiding/showing con SnackBar", "Fab extendido", "FAB anclado entre vistas")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { _, which ->
            when (which) {
                0 -> openActivity(FabConSnackYScroll::class.java)
                1 -> openActivity(FabExpandido::class.java)
                else -> openActivity(FabEntreVistas::class.java)
            }
        }

        builder.create().show()
    }

    private fun toolbarAlertDialog() {
        val lista = arrayOf("Toolbar son scroll", "Toolbar con snterAlways")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { _, which ->
            if (which == 0) openActivity(ToolbarScroll::class.java)
            else openActivity(ToolbarEnterAlways::class.java)
        }

        builder.create().show()
    }

    private fun colllapsingAlertDialog() {
        val lista = arrayOf(
            "Collapsing scroll|enterAlways",
            "Collapsing scroll|enterAlwaysCollapsed",
            "Collapsing scroll|enterAlways|enterAlwaysCollapsed",
            "Collapsing scroll|exitUntilCollapsed"
        )

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { _, which ->
            when (which) {
                0 -> openActivity(CollapseCase1::class.java)
                1 -> openActivity(CollapseCase2::class.java)
                2 -> openActivity(CollapseCase3::class.java)
                3 -> openActivity(CollapseCase4::class.java)
            }
        }

        builder.create().show()
    }

    private fun openActivity(clase: Class<*>) {
        startActivity(Intent(this, clase))
    }

}