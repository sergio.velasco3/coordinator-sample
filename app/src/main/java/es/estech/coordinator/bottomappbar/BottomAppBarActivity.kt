package es.estech.coordinator.bottomappbar

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.estech.coordinator.CheeseAdapter
import es.estech.coordinator.Cheeses
import es.estech.coordinator.R
import es.estech.coordinator.databinding.BottomAppbarBinding

class BottomAppBarActivity : AppCompatActivity() {

    private lateinit var binding: BottomAppbarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BottomAppbarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.bottomAppBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupRecyclerView(binding.recyclerview)

        binding.floatingActionButton.setOnClickListener {
            Toast.makeText(this@BottomAppBarActivity, "OK", Toast.LENGTH_SHORT).show()
        }

        addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_bottomappbar, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.app_bar_fav -> {
                        Toast.makeText(this@BottomAppBarActivity, "OK", Toast.LENGTH_SHORT).show()
                        true
                    }

                    R.id.app_bar_search -> {
                        Toast.makeText(this@BottomAppBarActivity, "Ayuda", Toast.LENGTH_SHORT)
                            .show()
                        true
                    }

                    R.id.app_bar_settings -> {
                        Toast.makeText(this@BottomAppBarActivity, "Favoritos", Toast.LENGTH_SHORT)
                            .show()
                        true
                    }

                    android.R.id.home -> {
                        finish()
                        true
                    }

                    else -> false
                }
            }

        })
    }

    /**
     * Método para configurar el recyclerview
     *
     * @param recyclerView -> recibe como parámetro el recyclerview a configurar
     */
    private fun setupRecyclerView(recyclerView: RecyclerView) {
        val listaQuesos = Cheeses.getRandomSublist(30)

        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = CheeseAdapter(listaQuesos)
    }
}