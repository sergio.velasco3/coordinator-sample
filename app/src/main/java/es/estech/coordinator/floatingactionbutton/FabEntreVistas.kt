package es.estech.coordinator.floatingactionbutton

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.estech.coordinator.databinding.FabEntreVistasBinding

class FabEntreVistas : AppCompatActivity() {

    private lateinit var binding: FabEntreVistasBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FabEntreVistasBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        setTitle("Fab entre vistas")
    }
}